package com.example.petbnb_caretakers

import android.Manifest
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.*
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.petbnb_caretakers.Models.Caretaker
import com.example.petbnb_caretakers.Utils.EventualConnectivityUtils
import com.example.petbnb_caretakers.Utils.FirebaseAnalyticsUtils
import com.example.petbnb_caretakers.databinding.ActivityProfileBinding
import com.example.petbnb_caretakers.databinding.ActivitySignUpBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.ByteArrayOutputStream
import java.util.*


class ProfileActivity : AppCompatActivity() , LocationListener {
    val fireBaseAnalyticsUtils: FirebaseAnalyticsUtils = FirebaseAnalyticsUtils()
    private lateinit var binding: ActivityProfileBinding
    var profilePictureUrl:String? = ""
    var imageUri: Uri? = null
    var sector: String? = ""
    var caretakers: ArrayList<Caretaker?> = ArrayList<Caretaker?>()
    var averageFeeByRating:Double = 0.0
    private lateinit var locationManager: LocationManager
    private val locationPermissionCode = 2
    private lateinit var gpsLocationTextView: TextView
    var started:Boolean = false
    var location:String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        fireBaseAnalyticsUtils.logOpenedScreenEvent("ProfileActivity")
        // Back button configuration
        binding.signupBackImageButtonView.setOnClickListener{
            finish()
        }

        // Profile button configuration
        checkPermissions(binding.profilePictureButton)

        binding.profilePictureButton.setOnClickListener{
            if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), locationPermissionCode)
                return@setOnClickListener
            }
            if ((ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), locationPermissionCode)
                return@setOnClickListener
            }
            val no = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(no, 101)
        }


        loadUserInfo()

        binding.profileUpdateButton.setOnClickListener{
            updateProfileInfo()
        }
        //updateProfileInfo()


        binding.profileSuggestButtonView.setOnClickListener{
            if (!getAvailableMemory().lowMemory) {
                suggestFee()
            }

        }

        binding.profileLocationImageButton.setOnClickListener{
            if (!getAvailableMemory().lowMemory) {
                Toast.makeText(this, "Fetching location. This might take a few seconds", Toast.LENGTH_LONG).show()
                runBlocking {
                    launch{
                        getLocation()
                    }
                }
            }


        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 101){
            fireBaseAnalyticsUtils.logUsedFunctionalityEvent("ChangeProfilePicture")
            val image:Bitmap? = data?.getParcelableExtra<Bitmap>("data")
            binding.profilePictureButton.setImageBitmap(image)
            binding.profileOverlappedImageView.alpha = 0f
            imageUri = getImageUri(this, image!!)
        }
    }

    fun hasPermissions(context: Context, vararg permissions: String): Boolean = permissions.all {
        ActivityCompat.checkSelfPermission(context, it) == PackageManager.PERMISSION_GRANTED
    }

    private fun loadUserInfo(){
        val sharedPref = this?.getSharedPreferences(
            getString(R.string.user_info_key),
            Context.MODE_PRIVATE
        )
        val sharedPreferencesAll = sharedPref.all
        Log.d("ProfileActivity", "The shared preference keys are: ${sharedPreferencesAll.keys}")
        Log.d("ProfileActivity", "The shared preference values are: ${sharedPreferencesAll.values}")
        val userImage: String? = sharedPreferencesAll.getValue(getString(R.string.user_image_key)).toString()
        val firstName:String? = sharedPreferencesAll.getValue(getString(R.string.first_name_key)).toString()
        val lastName:String? = sharedPreferencesAll.getValue(getString(R.string.last_name_key)).toString()
        val phoneNumber: String? = sharedPreferencesAll.getValue(getString(R.string.phone_number_key)).toString()
        val email: String? = sharedPreferencesAll.getValue(getString(R.string.email_key)).toString()
        val fee: String? = sharedPreferencesAll.getValue(getString(R.string.fee_key)).toString()
        val rating: String? = sharedPreferencesAll.getValue(getString(R.string.rating_key)).toString()
        location = sharedPreferencesAll.getValue(getString(R.string.location_key)).toString()
        sector = sharedPreferencesAll.getValue(getString(R.string.sector_key)).toString()

        Log.d("ProfileActivity", "Profile picture: $firstName")

        binding.profileFirstNameEditTextView.setText(firstName)
        binding.profileLastNameEditTextView.setText(lastName)
        binding.profilePhoneNumberEditTextView.setText(phoneNumber)
        binding.profileRatingTextView.text = "$rating/5"
        binding.profileFeeEditTextView.setText(fee)
        binding.profileLocationEditTextView.setText(location)


        if (userImage.toString().isNotEmpty()) {
            try {
                Picasso.get().load(userImage).into(binding.profileOverlappedImageView);
                val profileButton = findViewById<ImageView>(R.id.profile_image)
                profileButton.alpha = 0f
            } catch (e: Exception){
                //Toast.makeText(this, "Error: Could not retrieve image data", Toast.LENGTH_LONG).show()
                return
            }
        }
    }

    private fun getImageUri(context: Context, inImage: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path =
            MediaStore.Images.Media.insertImage(context.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }

    private fun uploadPhoto(uri: Uri?, firstName: String, lastName: String, phoneNumber: String, fee:String, location: String) {
        if (uri != null) {
            Log.d("ProfileActivity", "Entro a uploadPhoto")
            val filename = UUID.randomUUID().toString()
            val ref = FirebaseStorage.getInstance().getReference("/profile-pictures/${filename}")
            ref.putFile(uri).addOnSuccessListener {
                ref.downloadUrl.addOnSuccessListener {

                    profilePictureUrl = it.toString()
                    runBlocking {
                        launch(Dispatchers.IO) {
                            saveUserToFirebase(firstName, lastName, phoneNumber, fee, location)
                        }
                    }

                }
                return@addOnSuccessListener
                //Toast.makeText(this, "Image uploaded correctly", Toast.LENGTH_LONG).show()
            }.addOnFailureListener {
                Toast.makeText(this, "Error: ${it.localizedMessage}", Toast.LENGTH_LONG).show()
            }
        } else {
            runBlocking {
                launch(Dispatchers.IO) {
                    saveUserToFirebase(firstName, lastName, phoneNumber, fee, location)
                }
            }
        }


    }


    private fun getLocation(){
        fireBaseAnalyticsUtils.logUsedFunctionalityEvent("GetUserLocation")
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), locationPermissionCode)
            return
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 5f, this)
    }
    override fun onLocationChanged(location: Location) {
        this.location = location.latitude.toString() + ", " + location.longitude.toString()
        binding.profileLocationEditTextView.setText(location.latitude.toString() + ", " + location.longitude.toString())
        Toast.makeText(this, "Location fetched successfully", Toast.LENGTH_SHORT).show()
    }

    private fun updateProfileInfo(){
        val eventualConnectivity: EventualConnectivityUtils = EventualConnectivityUtils()
        val connected: Boolean = eventualConnectivity.checkInternetConnection(this)
        if(connected){

            val firstName = binding.profileFirstNameEditTextView.text.toString()
            val lastName = binding.profileLastNameEditTextView.text.toString()
            val phoneNumber = binding.profilePhoneNumberEditTextView.text.toString()
            val fee = binding.profileFeeEditTextView.text.toString()
            this.location = binding.profileLocationEditTextView.text.toString()
            val localLocation = this.location
            getSectorFromLocation()
            if(firstName.isEmpty() || lastName.isEmpty() || phoneNumber.isEmpty() || fee.isEmpty()){
                Toast.makeText(this, "Fill out all fields please", Toast.LENGTH_SHORT).show()
                return
            }
            runBlocking {
                launch(Dispatchers.IO){
                    uploadPhoto(imageUri, firstName, lastName, phoneNumber, fee, localLocation!!)
                }
            }
            //saveUserToFirebase(firstName, lastName, phoneNumber, fee, this.location!!)
        }else{
            val alertDialogBuilder = AlertDialog.Builder(this)
            alertDialogBuilder.setTitle("No Connection").setMessage("Check your internet connection or try again later").setPositiveButton(R.string.accept) { dialog, which ->
                return@setPositiveButton
            }
            alertDialogBuilder.show()
        }


    }
    private fun saveUserToFirebase(firstName: String, lastName: String, phoneNumber: String, fee:String, location:String){
        val uid = FirebaseAuth.getInstance().uid?:""
        val ref = FirebaseDatabase.getInstance().getReference("/caretakers/$uid")
        val sharedPref = this?.getSharedPreferences(
            getString(R.string.user_info_key),
            Context.MODE_PRIVATE
        )
        val sharedPreferencesAll = sharedPref.all
        val rating: String? = sharedPreferencesAll.getValue(getString(R.string.rating_key)).toString()
        if(!started){
            profilePictureUrl = sharedPreferencesAll.getValue(getString(R.string.user_image_key)).toString()
        }
        val caretaker = Caretaker(uid, firstName, lastName, phoneNumber, profilePictureUrl, rating, location, fee, sector)
        ref.setValue(caretaker)
            .addOnSuccessListener {
                if (started){
                    Toast.makeText(this, "User updated successfully", Toast.LENGTH_SHORT).show()
                } else {
                    started = true
                }
                fireBaseAnalyticsUtils.logUsedFunctionalityEvent("UpdateProfile")
                //suggestFee()
                binding.profileFeeEditTextView.setText(fee.toInt().toString())
                var averageFee = ((averageFeeByRating * (caretakers.size-1)) + (fee!!.toInt()/rating!!.toInt()))/caretakers.size
                var deltaFee = averageFee - averageFeeByRating
                fireBaseAnalyticsUtils.logNewFeeEvent(averageFee.toInt(), deltaFee.toInt())
            }.addOnFailureListener{
                Toast.makeText(this, "Error: ${it.localizedMessage}", Toast.LENGTH_SHORT).show()
            }

    }

    private fun getSectorFromLocation(){
            val geocoder = Geocoder(this, Locale.getDefault())
            val addresses: List<Address> = geocoder.getFromLocation(location!!.split(',')[0]?.toDouble(), location!!.split(',')[1]?.toDouble(), 1)
            val postalCode: String? = addresses[0].postalCode
            sector = postalCode
    }


    private fun checkPermissions(profileButton: ImageButton){

        val PERMISSION_ALL = 1
        val PERMISSIONS = arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION
        )

        if (!hasPermissions(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) || !hasPermissions(this, Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }


    }

    // Get a MemoryInfo object for the device's current memory status.
    public fun getAvailableMemory(): ActivityManager.MemoryInfo {
        val activityManager = getSystemService(ACTIVITY_SERVICE) as ActivityManager
        return ActivityManager.MemoryInfo().also { memoryInfo ->
            activityManager.getMemoryInfo(memoryInfo)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == 111 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
            binding.profilePictureButton.isEnabled = true
        }
        if(requestCode == 112 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
            binding.profilePictureButton.isEnabled = true
        }
        if (requestCode == locationPermissionCode) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show()
            }
            else {
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun suggestFee(){
        val eventualConnectivity:EventualConnectivityUtils = EventualConnectivityUtils()
        val connected: Boolean = eventualConnectivity.checkInternetConnection(this)
        if(connected){
        runBlocking {
                launch(Dispatchers.IO){
                    fetchSuggestedFeeInformation()
                }
        }

        }else{
            val alertDialogBuilder = AlertDialog.Builder(this)
            alertDialogBuilder.setTitle("No Connection").setMessage("Check your internet connection or try again later").setPositiveButton(R.string.accept) { dialog, which ->
                return@setPositiveButton
            }
            alertDialogBuilder.show()
        }
    }

    fun calculateSuggestFee(){
        val sharedPref = this?.getSharedPreferences(
            getString(R.string.user_info_key),
            Context.MODE_PRIVATE
        )
        averageFeeByRating = 0.0
        val sharedPreferencesAll = sharedPref.all
        val uid: String? = sharedPreferencesAll.getValue(getString(R.string.uid_key)).toString()
        var rating:Int = 1
        var caretaker:Caretaker?
        repeat(caretakers.size) { i -> // launch a few children jobs
            caretaker = caretakers[i]
            if(!caretaker!!.uid.equals(uid)){
                averageFeeByRating += caretaker!!.fee!!.toDouble()/caretaker!!.rating!!.toInt()
            }else{
                rating = caretaker!!.rating!!.toInt()
            }
        }
//        for (caretaker in caretakers){
//            if(!caretaker!!.uid.equals(uid)){
//                averageFeeByRating += caretaker!!.fee!!.toDouble()/caretaker!!.rating!!.toInt()
//            }else{
//                rating = caretaker!!.rating!!.toInt()
//            }
//
//        }
        if(caretakers.size>1){
            averageFeeByRating = averageFeeByRating/(caretakers.size-1)
            var suggestedFee = averageFeeByRating * rating
            //Toast.makeText(this, "Suggested fee calculated", Toast.LENGTH_SHORT).show()
            binding.profileFeeEditTextView.setText(suggestedFee.toInt().toString())
        }else{
            //Toast.makeText(this, "Try again later", Toast.LENGTH_SHORT).show()
        }
        fireBaseAnalyticsUtils.logUsedFunctionalityEvent("SuggestFee")
    }

    suspend fun fetchSuggestedFeeInformation(){

            var ref = FirebaseDatabase.getInstance().getReference("/caretakers").orderByChild("sector").equalTo(sector).limitToLast(5)
            ref.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(p0: DataSnapshot) {
                    //val reservations: ArrayList<Reservation?> = ArrayList<Reservation?>()
                    caretakers.clear()
                    var caretaker: Caretaker?
                    repeat(p0.children.count()) { i -> // launch a few children jobs
                        caretaker = p0.children.elementAt(i).getValue(Caretaker::class.java)
                        caretakers.add(caretaker)
                    }
//                    for (reservationSnapshot in p0.children) {
//                        caretaker = reservationSnapshot.getValue(Caretaker::class.java)
//                        Log.d("HomeActivity", caretaker!!.lastName)
//                        caretakers.add(caretaker)
//                    }
                    runBlocking {
                        launch(Dispatchers.Default)
                        {
                            calculateSuggestFee()
                        }
                    }
                }
                override fun onCancelled(p0: DatabaseError) {
                    Log.d("HomeActivity", "I won't happen")
                }
            })


    }

}