package com.example.petbnb_caretakers.Utils

import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.ParametersBuilder
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase

class FirebaseAnalyticsUtils {

    private val firebaseAnalytics = Firebase.analytics
    public fun logOpenedScreenEvent(screenName: String) {
        firebaseAnalytics.logEvent("OpenedScreen"){
            param("screen", screenName)
        }
    }

    public fun logUsedFunctionalityEvent(functionalityName: String){
        firebaseAnalytics.logEvent("FunctionalityTriggered"){
            param("functionality", functionalityName)
        }
    }

    public fun logFirebaseEvent(firebaseEvent: String, params: Bundle? ){
        firebaseAnalytics.logEvent(firebaseEvent, params)
    }

    public fun logNewFeeEvent(fee:Int, deltaFee: Int){
        firebaseAnalytics.logEvent("NewFee"){
            param("fee", fee.toLong())
            param("deltaFee", deltaFee.toLong())
        }
    }

    public fun logNewRatingEvent(averageRating: Double){
        firebaseAnalytics.logEvent("NewRating"){
            param("rating", averageRating.toLong())
        }
    }

    public fun logDemandRatioEvent(ratio: Double){
        firebaseAnalytics.logEvent("NewDemandRatio"){
            param("demandRatio", ratio.toLong())
        }
    }

    public fun logAcceptedDeclinedRatio(ratio: Double){
        firebaseAnalytics.logEvent("NewAcceptedDeclinedRatio"){
            param("ratioAcceptDecline", ratio.toLong())
        }
    }

    public fun logReservationEngagementRatio(ratio: Double){
        firebaseAnalytics.logEvent("NewReservationEngagementRatio"){
            param("engagementRatio", ratio.toLong())
        }
    }
}