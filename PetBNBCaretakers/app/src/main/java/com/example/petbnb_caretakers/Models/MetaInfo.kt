package com.example.petbnb_caretakers.Models

class MetaInfo (val caretakers:Int, val reservations:Int, val reservationsAccepted:Int, val reservationsDeclined: Int){
    constructor():this(0,0,0,0)
}