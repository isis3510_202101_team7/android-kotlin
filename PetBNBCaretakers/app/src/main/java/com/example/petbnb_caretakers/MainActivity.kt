package com.example.petbnb_caretakers

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.petbnb_caretakers.Utils.EventualConnectivityUtils
import com.example.petbnb_caretakers.Utils.FirebaseAnalyticsUtils
import com.example.petbnb_caretakers.databinding.ActivityMainBinding
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking


class MainActivity : AppCompatActivity() {
    val fireBaseAnalyticsUtils: FirebaseAnalyticsUtils = FirebaseAnalyticsUtils()
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        fireBaseAnalyticsUtils.logOpenedScreenEvent("LoginActivity")
        setContentView(view)
        //val loginButton = findViewById<Button>(R.id.login)
        binding.login.setOnClickListener {
            binding.login.isEnabled = false
            login()
        }

        //val signupTextView = findViewById<TextView>(R.id.signup_textview)
        binding.signupTextview.setOnClickListener{
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        //val loginButton = findViewById<Button>(R.id.login)
        binding.login.isEnabled = true
    }

    private fun login(){
        //val loginButton = findViewById<Button>(R.id.login)
        fireBaseAnalyticsUtils.logFirebaseEvent(FirebaseAnalytics.Event.LOGIN, null)
        fireBaseAnalyticsUtils.logUsedFunctionalityEvent("Login")
        //val email = findViewById<TextView>(R.id.editTextTextEmailAddress).text.toString()
        val email = binding.editTextTextEmailAddress.text.toString()
        //val password = findViewById<TextView>(R.id.editTextTextPassword).text.toString()
        val password = binding.editTextTextPassword.text.toString()
        if(email.isEmpty()||password.isEmpty()){
            binding.login.isEnabled = true
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
            return
        }

        val eventualConnectivity: EventualConnectivityUtils = EventualConnectivityUtils()
        val connected: Boolean = eventualConnectivity.checkInternetConnection(this)
        if(connected){
            runBlocking {
                launch(Dispatchers.IO){
                    loginWithFirebase(email, password)
                }
            }
        }else{
            binding.login.isEnabled = true
            val alertDialogBuilder = AlertDialog.Builder(this)
            alertDialogBuilder.setTitle("No Connection").setMessage("Check your internet connection or try again later").setPositiveButton(R.string.accept) { dialog, which ->
                return@setPositiveButton
            }
            alertDialogBuilder.show()
        }
    }

    fun loginWithFirebase(email:String, password:String){
        //val loginButton = findViewById<Button>(R.id.login)
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
            .addOnCompleteListener{
                if(!it.isSuccessful) return@addOnCompleteListener
                var uid = it.getResult()?.user?.uid
                val sharedPref = this?.getSharedPreferences(getString(R.string.user_info_key),Context.MODE_PRIVATE)?:return@addOnCompleteListener
                with(sharedPref.edit()){
                    putString(getString(R.string.uid_key), uid)
                    putString(getString(R.string.email_key), email)
                    commit()
                }
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)
            }
            .addOnFailureListener{
                binding.login.isEnabled = true
                Toast.makeText(this, "Incorrect credentials", Toast.LENGTH_SHORT).show()
                return@addOnFailureListener
            }
    }
}