package com.example.petbnb_caretakers
import AcceptedReservationListAdapter
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.util.isEmpty
import androidx.core.util.valueIterator
import androidx.fragment.app.Fragment
import com.example.petbnb_caretakers.Models.Reservation
import com.example.petbnb_caretakers.Utils.EventualConnectivityUtils
import com.example.petbnb_caretakers.Utils.FirebaseAnalyticsUtils
import com.example.petbnb_caretakers.databinding.FragmentHomeBinding
import com.example.petbnb_caretakers.databinding.FragmentMessagesBinding
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class AcceptedReservationsFragment : Fragment() {

    val fireBaseAnalyticsUtils: FirebaseAnalyticsUtils = FirebaseAnalyticsUtils()
    private var reservations: ArrayList<Reservation?> = ArrayList<Reservation?>()
    var reservationsSA: SparseArray<Reservation> = SparseArray()
    var listView: ListView? = null
    private var _binding: FragmentMessagesBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

//        val view: View = inflater.inflate(R.layout.fragment_messages, container, false)
        _binding = FragmentMessagesBinding.inflate(inflater, container, false)
        val view = binding.root
        fireBaseAnalyticsUtils.logOpenedScreenEvent("AcceptedReservationsFragment")
        runBlocking{
            launch(Dispatchers.IO){
                getReservations()
            }
        }
        // List view configuration
        Log.d("HomeFragment","No. Me están esperando")
        listView = binding.acceptedReservationsReservationListview

        // Makes anchor invisible
        val anchor = view.findViewById<ImageView>(R.id.accepted_reservations_invisible_anchor)
        binding.acceptedReservationsInvisibleAnchor.setImageBitmap(null)

        // FAB configuration
        val filter_fab = view.findViewById<FloatingActionButton>(R.id.accepted_reservations_fab)
        binding.acceptedReservationsFab.setOnClickListener{
            println("Esto no es")
            val popup = PopupMenu(view.context, binding.acceptedReservationsInvisibleAnchor)
            popup.menuInflater.inflate(R.menu.filter_menu, popup.menu)
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    when (item.itemId) {
                        R.id.filter_rating -> {
                            filterByRating()
                            Toast.makeText(view.context, "Filtered by rating", Toast.LENGTH_LONG)
                                .show()
                        }
                        R.id.filter_date -> {
                            filterByDate()
                            Toast.makeText(view.context, "Filtered by date", Toast.LENGTH_LONG)
                                .show()
                        }
                    }
                    binding.acceptedReservationsReservationListview.adapter = AcceptedReservationListAdapter(view.context, reservations)
                    binding.acceptedReservationsReservationListview.divider = null
                    return true
                }
            })
            popup.show();

        }



        //listView.adapter = HomeFragment.HomeFragmentAdapter(view.context, reservations)
        //listView.divider = null

        return view

    }

    override fun onStart() {
        super.onStart()
        binding.acceptedReservationsReservationListview.adapter = AcceptedReservationListAdapter(view!!.context, reservations)
        binding.acceptedReservationsReservationListview.divider = null
    }

    override fun onResume() {
        super.onResume()
        this.onStart()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    fun filterByDate() {
        this.reservations.sortBy { it!!.startDate }
        fireBaseAnalyticsUtils.logUsedFunctionalityEvent("FilterByDate")
    }

    fun filterByRating(){
        this.reservations.sortByDescending { it!!.rating }
        fireBaseAnalyticsUtils.logUsedFunctionalityEvent("FilterByRating")
    }

    companion object {
        fun newInstance(): HomeFragment = HomeFragment()
    }

    fun getReservations(){
        fireBaseAnalyticsUtils.logUsedFunctionalityEvent("GetReservations")
        val eventualConnectivity: EventualConnectivityUtils = EventualConnectivityUtils()
        val connected: Boolean = eventualConnectivity.checkInternetConnection(context)
        if(connected){
            val sharedPref = context?.getSharedPreferences(
                getString(R.string.user_info_key),
                Context.MODE_PRIVATE
            )
            val sharedPreferencesAll = sharedPref?.all
            val uid: String? = sharedPreferencesAll?.getValue(getString(R.string.uid_key)).toString()
            var ref = FirebaseDatabase.getInstance().getReference("/reservations/accepted").orderByChild("caretakerUid").equalTo(uid)
            ref.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(p0: DataSnapshot) {
                    //val reservations: ArrayList<Reservation?> = ArrayList<Reservation?>()
                    reservations.clear()
                    var i = 0
                    for (reservationSnapshot in p0.children) {
                        val reservation: Reservation? = reservationSnapshot.getValue(Reservation::class.java)
                        reservationsSA.append(i, reservation)
                        i++
                        reservations.add(reservation)
                    }
                    if(view!=null){
                        Log.d("AcceptedReservationsF","uuuh")
                        binding.acceptedReservationsReservationListview.adapter = AcceptedReservationListAdapter(view!!.context, reservations)
                        binding.acceptedReservationsReservationListview.divider = null
                    }

                    Log.d("HomeFragment","No me están esperando")


                }

                override fun onCancelled(p0: DatabaseError) {
                    Log.d("HomeActivity", "I won't happen")
                }
            })
        }else{
            ///if(reservations.isEmpty()){
            if(!reservationsSA.isEmpty()){
                reservations.clear()
                for(reservation in reservationsSA.valueIterator()){
                    reservations.add(reservation)
                }
            }else{
                if(view!=null) {
                    val alertDialogBuilder = AlertDialog.Builder(view!!.context)
                    alertDialogBuilder.setTitle("No Connection")
                        .setMessage("Check your internet connection or try again later")
                        .setPositiveButton(R.string.accept) { dialog, which ->
                            return@setPositiveButton
                        }
                    alertDialogBuilder.show()
                }
            }
            //}
        }


    }
}