package com.example.petbnb_caretakers
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.SparseArray
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.util.isEmpty
import androidx.core.util.valueIterator
import androidx.fragment.app.Fragment
import com.example.petbnb_caretakers.Adapters.ReservationListAdapter
import com.example.petbnb_caretakers.Models.Reservation
import com.example.petbnb_caretakers.Utils.EventualConnectivityUtils
import com.example.petbnb_caretakers.Utils.FirebaseAnalyticsUtils
import com.example.petbnb_caretakers.databinding.FragmentHomeBinding
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.*
import kotlin.collections.ArrayList



class HomeFragment : Fragment() {

    val fireBaseAnalyticsUtils: FirebaseAnalyticsUtils = FirebaseAnalyticsUtils()
    private var reservations: ArrayList<Reservation?> = ArrayList<Reservation?>()
    var reservationsSA: SparseArray<Reservation> = SparseArray()
    var listView:ListView? = null
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val view = binding.root
//        val view: View = inflater.inflate(R.layout.fragment_home, container, false)

        fireBaseAnalyticsUtils.logOpenedScreenEvent("HomeFragment")
        runBlocking{
            launch(Dispatchers.IO){
                getReservations()
            }
        }
        // List view configuration
        Log.d("HomeFragment","No. Me están esperando")
//        listView = view.findViewById<ListView>(R.id.reservation_listview)
        listView = binding.reservationListview

        // Makes anchor invisible
        val anchor = view.findViewById<ImageView>(R.id.invisible_anchor)
        binding.invisibleAnchor.setImageBitmap(null)

        // FAB configuration
        //val filter_fab = view.findViewById<FloatingActionButton>(R.id.fab)
        binding.fab.setOnClickListener{
            println("Esto no es")
//            view.findViewById(R.id.invisible_anchor)
            val popup = PopupMenu(view.context, binding.invisibleAnchor)
            popup.menuInflater.inflate(R.menu.filter_menu, popup.menu)
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    when (item.itemId) {
                        R.id.filter_rating -> {
                            filterByRating()
                            Toast.makeText(view.context, "Filtered by rating", Toast.LENGTH_LONG)
                                .show()
                        }
                        R.id.filter_date -> {
                            filterByDate()
                            Toast.makeText(view.context, "Filtered by date", Toast.LENGTH_LONG)
                                .show()
                        }
                    }
                    listView = view.findViewById<ListView>(R.id.reservation_listview)
                    binding.reservationListview.adapter = ReservationListAdapter(view.context, reservations)
                    binding.reservationListview.divider = null
                    return true
                }
            })
            popup.show();

        }



        //listView.adapter = HomeFragment.HomeFragmentAdapter(view.context, reservations)
        //listView.divider = null

        return view

    }

    override fun onStart() {
        super.onStart()
        binding.reservationListview.adapter = ReservationListAdapter(view!!.context, reservations)
        binding.reservationListview.divider = null
    }

    override fun onResume() {
        super.onResume()
        this.onStart()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    fun filterByDate() {
        this.reservations.sortBy { it!!.startDate }
        fireBaseAnalyticsUtils.logUsedFunctionalityEvent("FilterByDate")
    }

    fun filterByRating(){
        this.reservations.sortByDescending { it!!.rating }
        fireBaseAnalyticsUtils.logUsedFunctionalityEvent("FilterByRating")
    }

    companion object {
        fun newInstance(): HomeFragment = HomeFragment()
    }

     fun getReservations(){
            fireBaseAnalyticsUtils.logUsedFunctionalityEvent("GetReservations")
            val eventualConnectivity:EventualConnectivityUtils = EventualConnectivityUtils()
            val connected: Boolean = eventualConnectivity.checkInternetConnection(context)
            if(connected){
                val sharedPref = context?.getSharedPreferences(
                    getString(R.string.user_info_key),
                    Context.MODE_PRIVATE
                )
                val sharedPreferencesAll = sharedPref?.all
                val uid: String? = sharedPreferencesAll?.getValue(getString(R.string.uid_key)).toString()
                var ref = FirebaseDatabase.getInstance().getReference("/reservations/pending").orderByChild("caretakerUid").equalTo(uid)
                ref.addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(p0: DataSnapshot) {
                        reservations.clear()
                        //var i = 0
                        var reservation:Reservation?
                        repeat(p0.children.count()) { i ->
                            reservation = p0.children.elementAt(i).getValue(Reservation::class.java)
                            reservationsSA.append(i, reservation)
                            reservations.add(reservation)
                            }
//                        for (reservationSnapshot in p0.children) {
//                            reservation = reservationSnapshot.getValue(Reservation::class.java)
//                            reservationsSA.append(i, reservation)
//                            i++
//                            reservations.add(reservation)
//                        }
                        if(view!=null){
                            Log.d("HomeFragment","uuuh")
                            listView?.adapter = ReservationListAdapter(view!!.context, reservations)
                            listView?.divider = null
                        }
                        Log.d("HomeFragment","No me están esperando")


                    }

                    override fun onCancelled(p0: DatabaseError) {
                        Log.d("HomeActivity", "I won't happen")
                    }
                })
            }else{
                ///if(reservations.isEmpty()){
                if(!reservationsSA.isEmpty()){
                    reservations.clear()
                    for(reservation in reservationsSA.valueIterator()){
                        reservations.add(reservation)
                    }
                }else{
                    if(view!=null) {
                        val alertDialogBuilder = AlertDialog.Builder(view!!.context)
                        alertDialogBuilder.setTitle("No Connection")
                            .setMessage("Check your internet connection or try again later")
                            .setPositiveButton(R.string.accept) { dialog, which ->
                                return@setPositiveButton
                            }
                        alertDialogBuilder.show()
                    }
                }
                //}
            }


    }

}