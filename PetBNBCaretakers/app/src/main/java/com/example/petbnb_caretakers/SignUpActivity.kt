package com.example.petbnb_caretakers

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Debug
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.util.isEmpty
import androidx.core.util.valueIterator
import com.example.petbnb_caretakers.Models.Caretaker
import com.example.petbnb_caretakers.Models.MetaInfo
import com.example.petbnb_caretakers.Models.Reservation
import com.example.petbnb_caretakers.Utils.EventualConnectivityUtils
import com.example.petbnb_caretakers.Utils.FirebaseAnalyticsUtils
import com.example.petbnb_caretakers.databinding.ActivityMainBinding
import com.example.petbnb_caretakers.databinding.ActivitySignUpBinding
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.ArrayList

class SignUpActivity : AppCompatActivity() {

    val fireBaseAnalyticsUtils: FirebaseAnalyticsUtils = FirebaseAnalyticsUtils()
    var caretakers: ArrayList<Caretaker?> = ArrayList<Caretaker?>()
    var metainfo: MetaInfo = MetaInfo()
    private lateinit var binding: ActivitySignUpBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignUpBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        fireBaseAnalyticsUtils.logOpenedScreenEvent("SignUpActivity")

        binding.signupBackImageButtonView.setOnClickListener{
            finish()
        }

        binding.signUpButtonView.setOnClickListener{
            binding.signUpButtonView.isEnabled = false
            signUpUser()
        }

        calculateDemandRatio()
    }

    override fun onResume() {
        super.onResume()
        binding.signUpButtonView.isEnabled = true;
    }

    private fun signUpUser(){
        fireBaseAnalyticsUtils.logFirebaseEvent(FirebaseAnalytics.Event.SIGN_UP, null)
        fireBaseAnalyticsUtils.logUsedFunctionalityEvent("SignUp")
        val eventualConnectivity: EventualConnectivityUtils = EventualConnectivityUtils()
        val connected: Boolean = eventualConnectivity.checkInternetConnection(this)
        if(connected){
            val firstName = binding.firstnameEdittextview.text.toString()
            val lastName = binding.lastnameEdittextview.text.toString()
            val phoneNumber = binding.phonenumberEdittextview.text.toString()
            val password = binding.editTextTextPassword.text.toString()
            val emailAddress = binding.editTextTextEmailAddress.text.toString()
            val confirmPassword = binding.confirmpasswordEdittextview.text.toString()
            if(firstName.isEmpty() || lastName.isEmpty() || password.isEmpty() || emailAddress.isEmpty() || confirmPassword.isEmpty() || phoneNumber.isEmpty()){
                binding.signUpButtonView.isEnabled = true;
                Toast.makeText(this, "Fill out all fields please", Toast.LENGTH_SHORT).show()
                return
            }else if(!password.equals(confirmPassword)){
                binding.signUpButtonView.isEnabled = true;
                Toast.makeText(this, "Passwords don't match", Toast.LENGTH_SHORT).show()
                return
            }
            runBlocking {
                launch(Dispatchers.IO){
                    registerNewUserToFirebase(emailAddress, password, firstName, lastName, phoneNumber)
                }
            }

            binding.signUpButtonView.isEnabled = true;
        }else{
            val alertDialogBuilder = AlertDialog.Builder(this)
            alertDialogBuilder.setTitle("No Connection").setMessage("Check your internet connection or try again later").setPositiveButton(R.string.accept) { dialog, which ->
                return@setPositiveButton
            }
            alertDialogBuilder.show()
            binding.signUpButtonView.isEnabled = true;
        }

    }

    private fun registerNewUserToFirebase(emailAddress:String, password:String, firstName:String, lastName: String, phoneNumber: String){
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(emailAddress, password)
            .addOnCompleteListener{
                if(!it.isSuccessful) return@addOnCompleteListener
                runBlocking {
                    launch(Dispatchers.IO){
                        SaveUserToFirebase(firstName, lastName, phoneNumber, emailAddress)
                    }
                }

            }
            .addOnFailureListener{
                Toast.makeText(this, "Error: ${it.localizedMessage}", Toast.LENGTH_SHORT).show()
                binding.signUpButtonView.isEnabled = true;
                return@addOnFailureListener
            }
    }

    private fun SaveUserToFirebase(firstName: String, lastName:String, phoneNumber:String, email:String){
        val uid = FirebaseAuth.getInstance().uid?:""
        val ref = FirebaseDatabase.getInstance().getReference("/caretakers/$uid")
        val rating = (0..5).random()
        calculateAverageRating(rating)
        val fee = (3000..8000).random() * rating
        val caretaker = Caretaker(uid, firstName, lastName, phoneNumber, "", rating.toString(), "", fee.toString(), "")
         ref.setValue(caretaker)
             .addOnSuccessListener {
                 Toast.makeText(this, "User created successfully", Toast.LENGTH_SHORT).show()
                 val sharedPref = this?.getSharedPreferences(getString(R.string.user_info_key),
                     Context.MODE_PRIVATE)?: return@addOnSuccessListener
                 with(sharedPref.edit()){
                     putString(getString(R.string.uid_key), uid)
                     putString(getString(R.string.email_key), email)
                     commit()
                 }

                 val intent = Intent(this, HomeActivity::class.java)
                 startActivity(intent)
             }
        updateMetaInfo()
    }
    suspend fun getUsers(){

        var ref = FirebaseDatabase.getInstance().getReference("/caretakers")
        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                //val reservations: ArrayList<Reservation?> = ArrayList<Reservation?>()
                caretakers.clear()
                for (reservationSnapshot in p0.children) {
                    val caretaker: Caretaker? = reservationSnapshot.getValue(Caretaker::class.java)
                    caretakers.add(caretaker)
                }
            }
            override fun onCancelled(p0: DatabaseError) {
                Log.d("HomeActivity", "I won't happen")
            }
        })
        val sharedPref = this?.getSharedPreferences(
            getString(R.string.meta_info_key),
            Context.MODE_PRIVATE
        )
        with(sharedPref.edit()){
            putInt(getString(R.string.user_size_key), caretakers.size)
            commit()
        }
    }



    fun calculateAverageRating(rating: Int){
        val eventualConnectivity:EventualConnectivityUtils = EventualConnectivityUtils()
        val connected: Boolean = eventualConnectivity.checkInternetConnection(this)
        if(connected){
            runBlocking {
                    val async = async {
                        getUsers()
                    }
                    async.await()

            }
            var ratingSum: Double = 0.0
            var averageRating: Double = 0.0
            for (caretaker in caretakers){
                ratingSum += caretaker!!.rating!!.toDouble()
            }
            if(caretakers.size>0){
                averageRating = (ratingSum.toDouble() + rating)/(caretakers.size.toDouble()+1.0)
            }
            fireBaseAnalyticsUtils.logNewRatingEvent(averageRating)
        }else{
            Toast.makeText(this,"No internet connection. Try again later.", Toast.LENGTH_LONG).show()
        }
    }

    fun calculateDemandRatio(){
        var ref = FirebaseDatabase.getInstance().getReference("/meta-info")
        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                metainfo = p0.getValue(MetaInfo::class.java)!!
            }
            override fun onCancelled(p0: DatabaseError) {
                Log.d("HomeActivity", "I won't happen")
            }
        })
    }

    fun updateMetaInfo(){
        val ref = FirebaseDatabase.getInstance().getReference("/meta-info")
        val newMetaInfo = MetaInfo(metainfo.caretakers+1, metainfo.reservations, metainfo.reservationsAccepted, metainfo.reservationsDeclined)
        val ratio: Double = newMetaInfo.caretakers.toDouble()/newMetaInfo.reservations
        ref.setValue(newMetaInfo)
            .addOnSuccessListener {
                fireBaseAnalyticsUtils.logDemandRatioEvent(ratio)
            }
    }

}