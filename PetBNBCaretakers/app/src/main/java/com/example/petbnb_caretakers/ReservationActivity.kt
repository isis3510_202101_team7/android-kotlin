package com.example.petbnb_caretakers

import android.content.Context
import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.example.petbnb_caretakers.Models.MetaInfo
import com.example.petbnb_caretakers.Models.Reservation
import com.example.petbnb_caretakers.Utils.EventualConnectivityUtils
import com.example.petbnb_caretakers.Utils.FirebaseAnalyticsUtils
import com.example.petbnb_caretakers.databinding.ActivityHomeBinding
import com.example.petbnb_caretakers.databinding.ActivityReservationBinding
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class ReservationActivity : AppCompatActivity() {

    var owner:String? = ""
    var startDate:String? = ""
    var endDate:String? = ""
    var phoneNumber:String? = ""
    var rating:Int? = 0
    var comments:String? = ""
    var pet:String? = ""
    var uid:String? = ""
    var reservation:Reservation? = null
    var reservationId:String? = ""
    val firebaseAnalyticsUtils: FirebaseAnalyticsUtils = FirebaseAnalyticsUtils()
    var metainfo: MetaInfo = MetaInfo()
    private lateinit var binding: ActivityReservationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityReservationBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        firebaseAnalyticsUtils.logOpenedScreenEvent("ReservationActivity")
        owner = intent.getStringExtra("owner")
        startDate = intent.getStringExtra("startDate")
        endDate = intent.getStringExtra("endDate")
        phoneNumber = intent.getStringExtra("phoneNumber")
        rating = intent.getIntExtra("rating", 0)
        comments = intent.getStringExtra("comments")
        pet = intent.getStringExtra("pet")
        reservationId = intent.getStringExtra("id")
        val accepted:Boolean = intent.getBooleanExtra("accepted", false)
        if(accepted){
            binding.acceptButton.visibility = View.GONE
            binding.declineButton.visibility = View.GONE
        }

        val sharedPref = this.getSharedPreferences(
            getString(R.string.user_info_key),
            Context.MODE_PRIVATE
        )
        val sharedPreferencesAll = sharedPref.all
        uid= sharedPreferencesAll?.getValue(getString(R.string.uid_key)).toString()

        reservation = Reservation(comments, endDate, owner, pet, phoneNumber, rating, startDate, reservationId, uid)
        // List view configuration
        binding.reservationListview.adapter = ReservationActivity.ReservationAdapter(this, reservation!!)

        // Back button configuration
        binding.reservationBackImageButtonView.setOnClickListener{
            finish()
        }

        val eventualConnectivity: EventualConnectivityUtils = EventualConnectivityUtils()
        val connected: Boolean = eventualConnectivity.checkInternetConnection(this)
        binding.declineButton.setOnClickListener{
            if(connected){
                runBlocking {
                    launch(Dispatchers.IO){
                        declineReservation()
                    }
                }
            }else{
                showNoConnectionDialog()
            }

        }
        binding.acceptButton.setOnClickListener{
            if(connected){
                runBlocking {
                    launch(Dispatchers.IO){
                        acceptReservation()
                    }
                }
            }else{
                showNoConnectionDialog()
            }

        }
        getMetaInfo()
    }

    private fun showNoConnectionDialog(){
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle("No Connection").setMessage("Check your internet connection or try again later").setPositiveButton(R.string.accept) { dialog, which ->
            return@setPositiveButton
        }
        alertDialogBuilder.show()
    }

    private fun declineReservation(){

        firebaseAnalyticsUtils.logUsedFunctionalityEvent("DeclineRequest")
        updateMetaInfo(metainfo.reservationsAccepted, metainfo.reservationsDeclined+1)
            var ref =
                FirebaseDatabase.getInstance().getReference("/reservations/pending/$reservationId")
                    .removeValue().addOnSuccessListener {
                    Toast.makeText(this, "Reservation declined successfully", Toast.LENGTH_SHORT)
                        .show()
                    finish()
                }

    }

    private fun acceptReservation(){
        firebaseAnalyticsUtils.logUsedFunctionalityEvent("AcceptRequest")
        updateMetaInfo(metainfo.reservationsAccepted+1, metainfo.reservationsDeclined)
            var ref = FirebaseDatabase.getInstance().getReference("/reservations")
            ref.child("pending/$reservationId").removeValue().addOnSuccessListener {
                ref.child("accepted/$reservationId").setValue(reservation).addOnSuccessListener {
                    Toast.makeText(this, "Reservation accepted successfully", Toast.LENGTH_SHORT)
                        .show()
                    finish()
                }
            }

    }

    fun getMetaInfo(){
        var ref = FirebaseDatabase.getInstance().getReference("/meta-info")
        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                metainfo = p0.getValue(MetaInfo::class.java)!!
            }
            override fun onCancelled(p0: DatabaseError) {
                Log.d("HomeActivity", "I won't happen")
            }
        })
    }

    fun modifyAcceptanceDeclineRatio(acceptance:Int, decline:Int){
        var acceptanceDeclinedRatio:Double = acceptance.toDouble()/decline
        firebaseAnalyticsUtils.logAcceptedDeclinedRatio(acceptanceDeclinedRatio)
    }

    fun modifyEngagementRatio(acceptance:Int, decline:Int, reservations:Int){
        var engagementRatio:Double = (acceptance.toDouble()+decline)/reservations
        firebaseAnalyticsUtils.logReservationEngagementRatio(engagementRatio)
    }

    fun updateMetaInfo(reservationsAccepted:Int, reservationsDeclined:Int){
        val ref = FirebaseDatabase.getInstance().getReference("/meta-info")
        val newMetaInfo = MetaInfo(metainfo.caretakers, metainfo.reservations, reservationsAccepted, reservationsDeclined)
        ref.setValue(newMetaInfo)
            .addOnSuccessListener {
                modifyAcceptanceDeclineRatio(reservationsAccepted, reservationsDeclined)
                modifyEngagementRatio(reservationsAccepted, reservationsDeclined, metainfo.reservations)
            }
    }



    private class ReservationAdapter(context: Context, reservation: Reservation): BaseAdapter() {

        private val mContext: Context

        private val headers = arrayListOf<String>(
            "Pet",
            "Owner",
            "Date of arrival",
            "Departure date",
            "Additional information",
            "Phone number"
        )
        private val contents = arrayListOf<String?>(
            reservation.pet,
            reservation.owner,
            reservation.startDate,
            reservation.endDate,
            reservation.comments,
            reservation.phoneNumber
        )

        private val actions = arrayListOf<Boolean>(
            false,
            false,
            false,
            false,
            false,
            false
        )

        init {
            mContext = context
        }

        override fun getItemId(p0: Int): Long {
            return p0.toLong()
        }

        override fun getCount(): Int {
            return contents.size
        }

        override fun getItem(p0: Int): Any {
            return "String"
        }

        override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
            val layoutInflater = LayoutInflater.from(mContext)
            val row = layoutInflater.inflate(R.layout.row, viewGroup, false)
            val headerTextView = row.findViewById<TextView>(R.id.header_textview)
            val contentTextView = row.findViewById<TextView>(R.id.content_textview)
            val actionButton = row.findViewById<ImageButton>(R.id.action_imageButton)

            headerTextView.text = headers[position]
            contentTextView.text = contents[position]
            if (actions[position]!=true)
                actionButton.setImageBitmap(null)

            return row
        }



    }
}