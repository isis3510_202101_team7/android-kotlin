package com.example.petbnb_caretakers

import android.Manifest
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.petbnb_caretakers.Models.Caretaker
import com.example.petbnb_caretakers.Utils.EventualConnectivityUtils
import com.example.petbnb_caretakers.Utils.FirebaseAnalyticsUtils
import com.example.petbnb_caretakers.databinding.ActivityHomeBinding
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.*


class HomeActivity : AppCompatActivity() , LocationListener {
    companion object {
        var currentUser: Caretaker? = null
    }
    private lateinit var binding: ActivityHomeBinding

    private lateinit var locationManager: LocationManager
    val fireBaseAnalyticsUtils: FirebaseAnalyticsUtils = FirebaseAnalyticsUtils()
    private val locationPermissionCode = 2
    var userLocation:String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        fireBaseAnalyticsUtils.logOpenedScreenEvent("HomeActivity")
        checkPermissions()
        runBlocking {
            launch(Dispatchers.IO){
                getUserInfo()
            }
        }

        // Profile Activity Intent
        val profilePicture = findViewById<ImageView>(R.id.home_profilePicture)
        binding.homeProfilePicture.setOnClickListener {
            val intent = Intent(this, ProfileActivity::class.java)
            startActivity(intent)
        }

        // Bottom nav configuration

        val homeFragment = HomeFragment()
        val settingsFragment = SettingsFragment()
        val acceptedReservationsFragment = AcceptedReservationsFragment()

        binding.bottomNavigation.setOnNavigationItemSelectedListener {
            when(it.itemId){
//                R.id.action_settings -> {
//                    makeCurrentFragment(settingsFragment)
//                    binding.homeTitleTextView.text = "Settings"
//                }
                R.id.action_accepted -> {
                    makeCurrentFragment(acceptedReservationsFragment)
                    binding.homeTitleTextView.text = "Reservations"
                }
                R.id.action_home -> {
                    makeCurrentFragment(homeFragment)
                    binding.homeTitleTextView.text= "Pending"
                }
            }
            true
        }
        binding.bottomNavigation.selectedItemId = R.id.action_home
        makeCurrentFragment(homeFragment)

    }

    override fun onStart() {
        super.onStart()
        checkInternetConnection()
    }

    override fun onResume() {
        super.onResume()
        checkInternetConnection()
    }

    private fun checkPosition(){
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                locationPermissionCode
            )
            return
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5f, this)
    }

    override fun onLocationChanged(location: Location) {
        if(userLocation!=""){
            location.latitude.toString() + ", " + location.longitude.toString()
            val loc1 = Location("")
            loc1.latitude = location.latitude
            loc1.longitude = location.longitude

            val loc2 = Location("")
            loc2.latitude = userLocation!!.split(",")[0].toDouble()
            loc2.longitude = userLocation!!.split(",")[1].toDouble()

            var distanceInMeters = loc1.distanceTo(loc2)
            Log.d("HomeActivity", "Distance between coordinates: $distanceInMeters m")
            if (distanceInMeters >= 500){
                val alertDialogBuilder = AlertDialog.Builder(this)
                alertDialogBuilder.setTitle("Warning").setMessage("It seems you're far from home, please take into account that if you have a pet under your care you must be with it at all times.").setPositiveButton(R.string.accept) { dialog, which ->
                    return@setPositiveButton
                }
                alertDialogBuilder.show()
            }
        }
    }

    override fun onProviderEnabled(provider: String) {}

    override fun onProviderDisabled(provider: String) {}

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}

    private fun checkPermissions(){

        val PERMISSION_ALL = 1
        val PERMISSIONS = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION
        )

        if (!hasPermissions(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) || !hasPermissions(
                this,
                Manifest.permission.CAMERA
            )) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
    }

    fun hasPermissions(context: Context, vararg permissions: String): Boolean = permissions.all {
        ActivityCompat.checkSelfPermission(context, it) == PackageManager.PERMISSION_GRANTED
    }

    private fun checkInternetConnection(){
        val eventualConnectivity: EventualConnectivityUtils = EventualConnectivityUtils()
        val connected: Boolean = eventualConnectivity.checkInternetConnection(this)
        if(!connected){
            val alertDialogBuilder = AlertDialog.Builder(this)
            alertDialogBuilder.setTitle("No Connection").setMessage("Check your internet connection or try again later").setPositiveButton(R.string.accept) { dialog, which ->
                return@setPositiveButton
            }
            alertDialogBuilder.show()
        }
    }

    private fun getUserInfo(){
        val sharedPref = this?.getSharedPreferences(
            getString(R.string.user_info_key),
            Context.MODE_PRIVATE
        )
        fireBaseAnalyticsUtils.logUsedFunctionalityEvent("GetUserInfo")
        val sharedPreferencesAll = sharedPref.all
        val uid = sharedPreferencesAll.getValue(getString(R.string.uid_key)).toString()
        val email = sharedPreferencesAll.getValue(getString(R.string.email_key)).toString()


        var ref = FirebaseDatabase.getInstance().getReference("/caretakers/${uid}")
        Log.d("HomeActivity", "The ref: $ref")
        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                Log.d("HomeActivity", "I don't happen")
                HomeActivity.currentUser = p0.getValue(Caretaker::class.java)
                val firstName: String? = currentUser?.firstName
                val lastName: String? = currentUser?.lastName
                userLocation = currentUser?.location
                Log.d("HomeActivity", "first name: ${currentUser}")
                var fullName = firstName!!.split(" ")[0] + " " + lastName!!.split(" ")[0]

                Log.d("HomeActivity", "fullname is: $fullName")
                binding.homeUserName.text = fullName
                with(sharedPref.edit()) {
                    putString(
                        getString(com.example.petbnb_caretakers.R.string.first_name_key),
                        HomeActivity.currentUser?.firstName
                    )
                    putString(
                        getString(com.example.petbnb_caretakers.R.string.last_name_key),
                        HomeActivity.currentUser?.lastName
                    )
                    putString(
                        getString(com.example.petbnb_caretakers.R.string.phone_number_key),
                        HomeActivity.currentUser?.phoneNumber
                    )
                    putString(
                        getString(com.example.petbnb_caretakers.R.string.user_image_key),
                        HomeActivity.currentUser?.profilePictureUrl
                    )
                    putString(getString(com.example.petbnb_caretakers.R.string.email_key), email)
                    putString(
                        getString(com.example.petbnb_caretakers.R.string.fee_key),
                        currentUser?.fee
                    )
                    putString(
                        getString(com.example.petbnb_caretakers.R.string.rating_key),
                        currentUser?.rating
                    )
                    putString(
                        getString(com.example.petbnb_caretakers.R.string.location_key),
                        currentUser?.location
                    )
                    putString(getString(R.string.sector_key), currentUser?.sector)
                    commit()
                }
                if (!getAvailableMemory().lowMemory) {
                    checkPosition()
                }


            }

            override fun onCancelled(p0: DatabaseError) {
                Log.d("HomeActivity", "I won't happen")
            }
        })

    }

    // Get a MemoryInfo object for the device's current memory status.
    public fun getAvailableMemory(): ActivityManager.MemoryInfo {
        val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        return ActivityManager.MemoryInfo().also { memoryInfo ->
            activityManager.getMemoryInfo(memoryInfo)
        }
    }

    private fun makeCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.wrapper, fragment)
            commit()
        }


//    if (userImage.toString().isNotEmpty()) {
//        try {
//            val `is`: InputStream = URL(userImage.toString()).content as InputStream
//            Drawable.createFromStream(`is`, "src name")
//        } catch (e: Exception){
//            Toast.makeText(this, "Error: Could not retrieve image data", Toast.LENGTH_LONG).show()
//            return
//        }
//        val circleImageView = findViewById<CircleImageView>(R.id.profilepicture_circleimageview_home)
//        val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, Uri.parse(userImage))
//        val profileButton = findViewById<ImageButton>(R.id.profile_pictureButton)
//        profileButton.alpha = 0f
//        circleImageView.setImageBitmap(bitmap)
//    }

}