package com.example.petbnb_caretakers.Models

class Caretaker (val uid:String, val firstName:String, val lastName:String, val phoneNumber:String, val profilePictureUrl:String?, val rating:String?, val location:String?, val fee:String?, val sector:String?){

    constructor():this("","","","","", "", "", "", "")

}