package com.example.petbnb_caretakers.Models

class Reservation (val comments:String?, val endDate:String?, val owner:String?, val pet:String?, val phoneNumber:String?, val rating:Int?, val startDate:String?, val id:String?, val caretakerUid:String?){

    constructor():this("","","","", "1111111111", 1, "", "","")

}