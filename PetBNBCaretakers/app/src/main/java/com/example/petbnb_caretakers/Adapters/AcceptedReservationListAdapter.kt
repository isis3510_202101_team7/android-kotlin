import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.petbnb_caretakers.Models.Reservation
import com.example.petbnb_caretakers.R
import com.example.petbnb_caretakers.ReservationActivity

public class AcceptedReservationListAdapter(context: Context, reservations: ArrayList<Reservation?>): BaseAdapter() {

    private val mContext: Context

    private val mReservations: ArrayList<Reservation?>


    init {
        mContext = context
        mReservations = reservations
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return mReservations.size
    }

    override fun getItem(p0: Int): Any {
        return "String"
    }

    override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
        val layoutInflater = LayoutInflater.from(mContext)
        val row = layoutInflater.inflate(R.layout.reservations_listview, viewGroup, false)
        val owner = row.findViewById<TextView>(R.id.reservations_listview_owner)
        val pet = row.findViewById<TextView>(R.id.revervations_listview_pet)
        val rating = row.findViewById<TextView>(R.id.reservations_listview_rating)
        //val phoneNumber = row.findViewById<TextView>(R.id.reservations_listview_phoneNumber)
        val date = row.findViewById<TextView>(R.id.reservations_listview_date)
        val reservationPicture = row.findViewById<ImageView>(R.id.reservations_listview_petPicture)
        owner.text = mReservations[position]!!.owner
        pet.text = mReservations[position]!!.pet
        //phoneNumber.text = mReservations[position].mPhoneNumber
        date.text = mReservations[position]!!.startDate
        rating.text = mReservations[position]!!.rating.toString() + "/5"
        reservationPicture.setImageResource(R.drawable.logo)
        row.setOnClickListener{
            val intent = Intent(mContext, ReservationActivity::class.java)
            intent.putExtra("owner", mReservations[position]!!.owner)
            intent.putExtra("comments", mReservations[position]!!.comments)
            intent.putExtra("pet", mReservations[position]!!.pet)
            intent.putExtra("startDate", mReservations[position]!!.startDate)
            intent.putExtra("endDate", mReservations[position]!!.endDate)
            intent.putExtra("phoneNumber", mReservations[position]!!.phoneNumber)
            intent.putExtra("rating", mReservations[position]!!.rating)
            intent.putExtra("id", mReservations[position]!!.id)
            intent.putExtra("accepted", true)
            mContext.startActivity(intent)
        }
        return row
    }



}